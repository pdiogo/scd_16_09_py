# Ubiwhere's SCD_16_09 - Python (DRF) version #

## Postman Collection
[![Run in Postman](https://run.pstmn.io/button.svg)](https://app.getpostman.com/run-collection/ba1a4ce958c4472a4ebc)

## Server

API is online at [http://ec2-54-93-121-26.eu-central-1.compute.amazonaws.com/](http://ec2-54-93-121-26.eu-central-1.compute.amazonaws.com/) (embedded Swagger spec)

**This is a very basic EC2 Instance, launched for *testing purposes only*. It is *not properly configured to work in Production mode*! Debug is set to `True`**.

### Authentication

Basic Authentication was implemented and no resource can be consumed without proper Auth. To do so, use the following user & password credentials:
```
username: pedro
password: learn-shock-rouse-latch
``` 

## Running locally

1. Clone/download this repo
    ```
    git clone https://pdiogo@bitbucket.org/pdiogo/scd_16_09_py.git
    ```

2. Make sure Python (tested with 2.7.x) is installed

3. Install dependencies 

    ```
    pip install -r requirements.txt
    ```

3. Run 

    ```
    python manage.py makemigrations api && python manage.py migrate && python manage.py runserver
    ```

## Documentation

The RESTful API was documented using the [Swagger specification](http://swagger.io/), which is served at the root (`http://$host:$port/`)

### Models & Services design

There are 5 different services in this API, each with their own Model:

* parameters

* events

* rules

* users

* alerts


All of them have proper CRUD methods, and some are related to each other. Visiting the API documentation helps to visualize this. 
There are also some linked resources:

`/users/{id}/rules`
&
`/users/{id}/alerts`

The supoprted methods are properly documented.

## TODO

- [x] Add AA (only Basic Authentication was implemented, no Authorization yet)
- [] Better error-handling (make use of Python's try-except)
- [] Properly launch in Production mode (use HTTPs only, CORS, static files, real DB, use PM, etc.) 
- [] Add all possible responses to Swagger spec
- [] Check all `TODO` tags @ code