# Response used in class-based views
from rest_framework.response import Response
from rest_framework.reverse import reverse


# Defining class-based views
def api_root(request, format=None):
    return Response({
        # reverse function needed to return fully-qualified URLs
        'parameters': reverse('parameter-list', request=request, format=format),
        'events': reverse('event-list', request=request, format=format),
        'rules': reverse('rules-list', request=request, format=format),
        'users': reverse('users-list', request=request, format=format)
    })
