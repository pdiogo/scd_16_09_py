# Needed Models
from api.models import Alert, Event, Rule, User
# Needed Serializers
from api.serializers import EventSerializer
# Using viewsets for abstraction purposes
#(http://www.django-rest-framework.org/tutorial/6-viewsets-and-routers/)
from rest_framework import viewsets


class EventViewSet(viewsets.ModelViewSet):
    """
    retrieve:
        Return a *Event* instance.

    list:
        Return all *Events*, ordered by most recently created.

    create:
        Create a new *Event*.

        When an *Event* is created, an evaluation to all users' rules is made:
            If there is a match with any rule(s) in place, `rules * users` *Alert*(s) is/are saved.
            These *Alert* may not actually be created, **if** the combination of `user` and `rule` already exists.
            If it does, a simple partial update is made, by adding the `event` to that *Alert*'s `events` list.

    delete:
        Remove an existing *Event*.

        Due to cascading, when an *Event* is deleted, it is also deleted from the *Alert*(s) where
        it was listed (`events`).

    partial_update:
        Update one or more fields on an existing *Event*.

    update:
        Update a *Event*.
    """
    queryset = Event.objects.all()
    serializer_class = EventSerializer

    def perform_create(self, serializer):
        """
        When creating an event, check *afterwards* if it triggers any rule associated with any user.

        Verifications:
            * Check all users where associated `rules` (M:M) should be triggered (make use of __ to populate the `rule` key)
                - If exists, then Alerts are to be set:
                    - Add `event.id` to Alert if it exists (`rule` and `user` are unique_together)
                    - If Alert does not exist, create it and then add `event.id` to it
                - If nothing was triggered, do nothing
        """
        event = serializer.save()
        # start by determining if this event triggers any rule(s) associated to
        # any user(s)
        users = User.objects.filter(rules__threshold__lte=serializer.initial_data['value'],
                                    rules__parameter_id=serializer.initial_data['parameter_id'])
        if users.exists():
            for user in users.all():
                print "Going to create Alert(s)"
                # loop to determine which ruleId
                for rule in user.rules.all():
                    if (rule.threshold <= serializer.initial_data['value'] and
                            rule.parameter_id.id == serializer.initial_data['parameter_id']):
                        # save this rule.id, event.id and user.id, but
                        # check first if there are no Alerts for rule.id &
                        # user.id.
                        alerts = Alert.objects.filter(
                            rule_id=rule.id, user_id=user.id)
                        print "alert QuerySet:", alerts
                        if alerts.exists():
                            for alert in alerts.all():
                                # Alert exists for this `rule` and `user` pair.
                                # Just add `event.id` to it.
                                alert.events.add(event.id)
                                alert.save()
                        else:
                            # Alert does not exist. Create it and add
                            # `event.id` to it.
                            alert = Alert(
                                parameter_id=rule.parameter_id,
                                rule_id=rule,
                                user_id=user
                            )
                            alert.save()
                            alert = Alert.objects.get(pk=alert.id)
                            alert.events.add(event.id)
                            alert.save()
        else:
            print "NOT Going to create Alerts"
