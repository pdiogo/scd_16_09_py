# Needed Models
from api.models import Rule, User
# Needed Serializer
from api.serializers import RuleSerializer
# Using viewsets for abstraction purposes
#(http://www.django-rest-framework.org/tutorial/6-viewsets-and-routers/)
from rest_framework import viewsets


class RuleViewSet(viewsets.ModelViewSet):
    """
    retrieve:
        Return a *Rule* instance.

    list:
        Return all *Rule*, ordered by most recently created.

    create:
        Create a new *Rule*.

        * After creating *Rule*, resulting id is added/replaced on `user_id`. 
        If *User* already has a *Rule* (in `rules` list) for this *Parameter* (`parameter_id`), then replace it with this new one.

    delete:
        Remove an existing *Rule*.

        Due to cascading, when a *Rule* is deleted, it is also deleted from the *User*(s) where
        it was listed (`rules`) and all linked *Alert*s are also deleted.

    partial_update:
        Update one or more fields on an existing *Rule*.

    update:
        Update a *Rule*.
    """
    queryset = Rule.objects.all()
    serializer_class = RuleSerializer


    """
    This func just adds/sets a rule to an User
    """
    def add_rule_to_user(self, user, parameter_id, rule_id):
        print "add_rule_to_user"
        found = False
        try:
            for rule in user.rules.all().values():
                # check if existing `parameter_id` is the same as the incoming
                if rule['parameter_id_id'] == parameter_id:
                    print "user has a rule for this parameter. will override"
                    user.rules.remove(rule['id'])
                    user.rules.add(rule_id)
                    found = True
                    break
            if found is False:
                print "user has no rules for this parameter. will add it"
                # user has no rules for this param, so just add it
                user.rules.add(rule_id)
        except:
            # TODO - send back this possible error (& make use of Python
            # exceptions)
            print "ERROR adding/setting rule to User"

    

    def perform_create(self, serializer):
        """
        This save hook is used to determine whether we want to create and
        add/set the rule to a user, or just create it.
        """

        saved_rule = serializer.save()

        # add/set this Rule to User
        user = User.objects.get(pk=serializer.initial_data['user_id'])
        self.add_rule_to_user(user, serializer.initial_data[
                            'parameter_id'], saved_rule.id)
