# Needed Model
from api.models import Parameter
# Needed Serializer
from api.serializers import ParameterSerializer
# Using viewsets for abstraction purposes
#(http://www.django-rest-framework.org/tutorial/6-viewsets-and-routers/)
from rest_framework import viewsets


class ParameterViewSet(viewsets.ModelViewSet):
    """
    retrieve:
        Return a *Parameter* instance.

    list:
        Return all *Parameters*, ordered by most recently created.

    create:
        Create a new *Parameter*.

    delete:
        Remove an existing *Parameter*.

        Due to cascading, when a **Parameter** is deleted, other resources are also deleted  (*Alert*, *Rule*, *Event*).
        As a result, these deleted resources might always be removed from any linked resource's list, if they are stored as Foreign Key
        (example: a deleted **Parameter** could cause a set of deletions on *Rule*, which in turn would also delete any *rule* id to be removed from a linked *User*'s `rules` list)

    partial_update:
        Update one or more fields on an existing *Parameter*.

    update:
        Update a *Parameter*.
    """
    queryset = Parameter.objects.all()
    serializer_class = ParameterSerializer
