# Needed Model
from api.models import User
# Needed Serializer
from api.serializers import UserSerializer
# Using viewsets for abstraction purposes
#(http://www.django-rest-framework.org/tutorial/6-viewsets-and-routers/)
from rest_framework import viewsets


class UserViewSet(viewsets.ModelViewSet):
    """
    retrieve:
        Return a *User* instance.

    list:
        Return all *Users*, ordered by most recently created.

    create:
        Create a new *User*.

    delete:
        Remove an existing *User*.

        Due to cascading, when an **User** is deleted, all linked *Alert*(s) are also deleted.

    partial_update:
        Update one or more fields on an existing *User*.

    update:
        Update a *User*.
    """
    queryset = User.objects.all()
    serializer_class = UserSerializer
