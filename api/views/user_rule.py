# Our Models
from api.models import Parameter, Rule, User
# Our Serializers
from api.serializers import UserRuleSerializer
# Needed ViewSet
from api.views import RuleViewSet
# Handy Django shortcuts
from django.shortcuts import get_object_or_404
# Using viewsets for abstraction purposes
#(http://www.django-rest-framework.org/tutorial/6-viewsets-and-routers/)
from rest_framework import viewsets
# Mixins for nested resources
from rest_framework.mixins import (CreateModelMixin, DestroyModelMixin,
                                   ListModelMixin)


class NestedRuleViewSet(CreateModelMixin, ListModelMixin, DestroyModelMixin, viewsets.GenericViewSet):
    """
    list:
        Nested Route returns all current *Rules* of a specific *User*.

    create:
        Nested Route creates a new *Rule* and automatically links it to *User*.

        ** Verifications **
            * Checking first if incoming `user_id` and `parameter_id` exist (if not, 404)
            * Same as in `POST /rules` with a `user_id`: 
            once the rule is saved, it is added to that *User*
            (if *User* already has a *Rule* for the same *Parameter*, it will be replaced)

    delete:
        Remove an existing *Rule* from a *User*.

        Same as DELETE /rules/{id}
    """
    queryset = Rule.objects.all()
    serializer_class = UserRuleSerializer

    def perform_create(self, serializer):
        # Before creating new rule, check if incoming parameter and user exists
        user = get_object_or_404(User, pk=self.kwargs['nested_1_pk'])
        parameter = get_object_or_404(Parameter, pk=serializer.initial_data['parameter_id'])
        # Create the rule
        rule = Rule(
            threshold=serializer.initial_data['threshold'],
            user_id=user,
            parameter_id=parameter
        )
        # Save the rule
        saved_rule = rule.save()
        # Add resulting Rule id to user (re-use existing func from RuleViewSet)
        # TODO - return created rule
        RuleViewSet.add_rule_to_user(RuleViewSet(), user, parameter.id, rule.id)

    def get_user(self, request, nested_1_pk=None):
        """
        Look for the referenced user first (if does not exist, return 404)
        """
        # Get the referenced user
        user = get_object_or_404(User, pk=nested_1_pk)
        return user

    def get_queryset(self):
        """
        Returns the queryset that will be used to retrieve the object that this view will display."
        (from docs)

        Gets the user's (passed from kwargs) rules, to use in the query filter.
        """

        # get the current user `rules`, so we can filter out any old Alerts
        # saved by old rules.
        user = User.objects.get(pk=self.kwargs['nested_1_pk'])
        user_rules = []
        for rule in user.rules.all():
            user_rules.append(rule.id)

        return Rule.objects.filter(id__in=user_rules)


    def list(self, request, *args, **kwargs):
        self.get_user(request, nested_1_pk=kwargs['nested_1_pk'])

        return super(NestedRuleViewSet, self).list(request, *args, **kwargs)

    def destroy(self, request, *args, **kwargs):
        self.get_user(request, nested_1_pk=kwargs['nested_1_pk'])

        return super(NestedRuleViewSet, self).destroy(request, *args, **kwargs)
