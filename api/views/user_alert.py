# Our Models
from api.models import Alert, User
# Our Serializers
from api.serializers import AlertSerializer, UserAlertSerializer
# Handy Django shortcuts
from django.shortcuts import get_object_or_404
# Using viewsets for abstraction purposes
#(http://www.django-rest-framework.org/tutorial/6-viewsets-and-routers/)
from rest_framework import viewsets
# Mixins for nested resources
from rest_framework.mixins import CreateModelMixin, ListModelMixin
from rest_framework.response import Response


class NestedAlertViewSet(ListModelMixin, viewsets.GenericViewSet):
    """
    list:
        Nested Route returns all *Alerts* of a specific *User*, filtered by that User's current `rules`.

        Returns an array of objects, ordered by *Parameter*s. Each *Parameter* contains an array of *Events* which 
        have triggered the *Alarm*. Formated so it is simpler to access `threshold` and know exactly when the `Alarm``
        was created.
    """
    queryset = Alert.objects.all()
    serializer_class = UserAlertSerializer

    def get_user(self, request, user_pk=None):
        """
        Look for the referenced user first (if does not exist, return 404)
        """
        # Get the referenced user
        user = get_object_or_404(User, pk=user_pk)
        return user

    def get_queryset(self):
        """
        Returns the queryset that will be used to retrieve the object that this view will display."
        (from docs)

        Gets the user's (passed from kwargs) rules, to use in the query filter.
        """

        # get the current user `rules`, so we can filter out any old Alerts
        # saved by old rules.
        user = User.objects.get(pk=self.kwargs['user_pk'])
        user_rules = []
        for rule in user.rules.all():
            user_rules.append(rule.id) 
        return Alert.objects.filter(user_id=self.kwargs['user_pk'], rule_id__in=user_rules)

    def list(self, request, *args, **kwargs):
        self.get_user(request, user_pk=kwargs['user_pk'])

        return super(NestedAlertViewSet, self).list(request, *args, **kwargs)
