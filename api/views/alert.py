from rest_framework import viewsets

from api.models import Alert, Event, Parameter, Rule, User
from api.serializers import (AlertSerializer, EventSerializer,
                             ParameterSerializer, RuleSerializer,
                             UserSerializer)


class AlertViewSet(viewsets.ModelViewSet):
    # TODO - check if I can disbable POST from outside requests
    """
    retrieve:
        Return an *Alert* instance.

    list:
        Return all *Alerts*, ordered by most recently created.

    create:
        Create a new *Alert*.

    delete:
        Remove an existing *Alert*.

    partial_update:
        Update one or more fields on an existing *Alert*.

    update:
        Update an *Alert*.
    """
    
    queryset = Alert.objects.all()
    serializer_class = AlertSerializer
