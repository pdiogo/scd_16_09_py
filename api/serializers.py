from rest_framework import serializers
from api.models import Parameter, Event, Rule, User, Alert

# TODO
#   - Use 'django-rest-framework-bulk' (https://github.com/miki725/django-rest-framework-bulk)
#   to support bulk_create with, at least, `Rule` and `Users`.
#   (don't forget to tweak views, afterwards)


class ParameterSerializer(serializers.ModelSerializer):

    class Meta:
        model = Parameter
        fields = ('id', 'name', 'created')
        name = serializers.CharField(required=True)


class EventSerializer(serializers.ModelSerializer):

    class Meta:
        model = Event
        fields = ('id', 'created', 'value', 'parameter_id')
        value = serializers.IntegerField(required=True)


class RuleSerializer(serializers.ModelSerializer):

    class Meta:
        model = Rule
        fields = ('id', 'created', 'threshold', 'parameter_id', 'user_id')
        threshold = serializers.IntegerField(required=True)


class AlertSerializer(serializers.ModelSerializer):
    
    class Meta:
        model = Alert
        fields = ('id', 'created', 'rule_id', 'user_id', 'parameter_id', 'events')


class UserSerializer(serializers.ModelSerializer):

    class Meta:
        model = User
        fields = ('id', 'created', 'email', 'rules')
        email = serializers.EmailField(required=True)


"""
Serializer for /users/{id}/alerts 
This list makes use of all data coming from `UserAlertSerializer`
"""
class UserAlertListSerializer(serializers.ListSerializer):
    def to_representation(self, data): # data is a list of Alert (model) instance(s)
        result = []
        for alert in data:
            formated_alert = {
                'created': None,
                'threshold': None,
                'parameter': None,
                'events': [] 
            }
            formated_alert['created'] = alert.created
            formated_alert['threshold'] = Rule.objects.get(pk=alert.rule_id_id).threshold
            formated_alert['parameter'] = Parameter.objects.get(pk=alert.parameter_id_id).name
            for event in alert.events.all():
                formated_alert['events'].append({
                    'created' : event.created,
                    'value': event.value
                })
            result.append(formated_alert)
        
        return result

"""
Same Serializer as `Alert`, but we're indicating a list_serializer_class,
which will then properly format the data  
"""
class UserAlertSerializer(serializers.ModelSerializer):
    class Meta:
        model = Alert
        fields = ('id', 'created', 'rule_id', 'parameter_id', 'events')
        list_serializer_class = UserAlertListSerializer #the serializer class used to serialize output


class UserRuleSerializer(serializers.ModelSerializer):

    class Meta:
        model = Rule
        fields = ('id', 'created', 'threshold', 'parameter_id')
        threshold = serializers.IntegerField(required=True)