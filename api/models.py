from __future__ import unicode_literals

from django.db import models


# Defining Models

class Parameter(models.Model):
    created = models.DateTimeField(auto_now_add=True)
    name = models.CharField(max_length=100, blank=False,
                            unique=True)  
    class Meta:
        ordering = ('created',)


class Event(models.Model):
    created = models.DateTimeField(auto_now_add=True)
    value = models.IntegerField()
    parameter_id = models.ForeignKey(Parameter, on_delete=models.CASCADE)

    class Meta:
        ordering = ('created',)


class Rule(models.Model):
    created = models.DateTimeField(auto_now_add=True)
    threshold = models.IntegerField(blank=False)
    parameter_id = models.ForeignKey(Parameter)
    user_id = models.ForeignKey('User')

    class Meta:
        ordering = ('created',)



class User(models.Model):
    created = models.DateTimeField(auto_now_add=True)
    email = models.EmailField(blank=False, unique=True)
    rules = models.ManyToManyField(Rule, blank=True)

    class Meta:
        ordering = ('created',)



class Alert(models.Model):
    created = models.DateTimeField(auto_now_add=True)
    parameter_id = models.ForeignKey(Parameter, on_delete=models.CASCADE)
    rule_id = models.ForeignKey(Rule, on_delete=models.CASCADE)
    user_id = models.ForeignKey(User, on_delete=models.CASCADE)
    events = models.ManyToManyField(Event, blank=True)

    class Meta:
        ordering = ('created',)
        unique_together = (("rule_id", "user_id"),)
