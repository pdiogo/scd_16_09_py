from django.conf.urls import include, url
from rest_framework.routers import DefaultRouter
# rest_framework_nested: https://github.com/alanjds/drf-nested-routers
# needed to support nested routes
from rest_framework_nested.routers import NestedSimpleRouter
from rest_framework_swagger.views import get_swagger_view

from api import views

# Swagger view
schema_view = get_swagger_view(title='SCD_16_09 (Django version)')

# Create a router and register our viewsets with it.
router = DefaultRouter()
router.register(r'parameters', views.parameter.ParameterViewSet)
router.register(r'events', views.event.EventViewSet)
router.register(r'rules', views.rule.RuleViewSet)
router.register(r'users', views.user.UserViewSet)
router.register(r'alerts', views.alert.AlertViewSet)

# users - alerts Nested Route
users_alerts_router = NestedSimpleRouter(router, r'users', lookup='user')
users_alerts_router.register(r'alerts', views.NestedAlertViewSet)

# users - rules Nested Route
users_rules_router = NestedSimpleRouter(router, r'users') #there is no lookup here, as Rule has no `user`
users_rules_router.register(r'rules', views.NestedRuleViewSet)

# The API URLs are now determined automatically by the router.
urlpatterns = [
    url(r'^$', schema_view),
    url(r'^api/v1/', include(router.urls, namespace='v1')),
    url(r'^api/v1/', include(users_alerts_router.urls, namespace='v1')),
    url(r'^api/v1/', include(users_rules_router.urls, namespace='v1')),
]
